---------------------------------------------------------------------------------
--Author: Wesley Nguyen
--Date: December 9, 2018
--File Name: bcd_counter.vhd
--Purpose of Code: Counter that displays on a Seven-Seg Display
--Project Part Number: Lab 3, Part 1
--Hardware FPGA/CPLD Device: xc7a100tcsg324-1
---------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity bcd_counter is
port(clk, load, updown:             in std_logic;
     din:                           in std_logic_vector (3 downto 0);
     clkout:                        out std_logic;
     cntout, led:                   out std_logic_vector (3 downto 0);
     dig:                           out std_logic_vector (7 downto 0));
end bcd_counter;

architecture behavioral of bcd_counter is
--internal signals
signal cnt_div:                     std_logic_vector (28 downto 0);
signal count:                       std_logic_vector (17 downto 0);
signal cnt, dd:                     std_logic_vector (3 downto 0);
signal an, segment:                 std_logic_vector (7 downto 0);
signal clk2:                        std_logic;

begin

process(clk)
begin
    if rising_edge (clk) then
        if(cnt_div = 99_999_999) then
            cnt_div <= (others => '0');
            clk2 <= '1';
        elsif (cnt_div < 49_999_999) then
            cnt_div <= cnt_div + 1;
            clk2 <= '1';
        else
            cnt_div <= cnt_div + 1;
            clk2 <= '0';
        end if;
    end if;
end process;

process (clk2, load, updown) --process active on event of clk2,load, or updown
begin
    if(load = '1') then
        cnt <= din;
    elsif rising_edge (clk2) then
        if(updown = '1') then
            if (cnt = "1001") then
                cnt <= "0000";
            else        
                cnt <= cnt + 1;
            end if;
        else
            if (cnt = "0000") then
                cnt <= "1001";
            else
                cnt <= cnt - 1;
            end if;    
        end if; 
    end if;
    segment(7) <= '1'; 
    if(rising_edge(clk2)) then
        case(cnt) is 
        when "0000" => segment(6 downto 0) <= "1000000"; --to display 0
        when "0001" => segment(6 downto 0) <= "1111001"; --to display 1
        when "0010" => segment(6 downto 0) <= "0100100"; --to display 2
        when "0011" => segment(6 downto 0) <= "0110000"; --to display 3
        when "0100" => segment(6 downto 0) <= "0011001"; --to display 4
        when "0101" => segment(6 downto 0) <= "0010010"; --to display 5
        when "0110" => segment(6 downto 0) <= "0000010"; --to display 6
        when "0111" => segment(6 downto 0) <= "1111000"; --to display 7
        when "1000" => segment(6 downto 0) <= "0000000"; --to display 8
        when "1001" => segment(6 downto 0) <= "0010000"; --to display 9
        when others => segment(6 downto 0) <= "1111111"; --blank
        end case;  
    end if;
end process;

process (clk)
begin
    if (rising_edge (clk)) then
        count <= count + 1;
        an  <= x"FD"; 
        case(count(17 downto 15)) is
        when "000" => dd  <= "0111"; 
        when "001" => dd  <= "0110";
        when "010" => dd  <= "0101";
        when "011" => dd  <= "0100";
        when "100" => dd  <= "0011";
        when "101" => dd  <= "0010";
        when "110" => dd  <= "0001";
        when "111" => dd  <= "0000";
        when others => dd  <= "0000";
        end case;
    end if;
end process;

dig <= an;

cntout <= cnt; --output cnt
clkout <= clk2;--output clk
led <= cnt; --led output value
end behavioral;