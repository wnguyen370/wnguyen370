//////////////////////////////////////////////////////////////////////////////////
//Author: Wesley Nguyen
//Date: November 25, 2018
//File Name: ADFF.v
//Purpose of Code: D-Flip-Flop for input A
//Project Part Number: Lab 2, Part 2, Sub-Part 4
//Hardware FPGA/CPLD Device: xc7a100tcsg324-1
//////////////////////////////////////////////////////////////////////////////////
module reg_a(clk, start, reset, load, data, dout);
input       start, reset, clk, load;
input       [3:0] data;
output      [3:0] dout;

reg         [3:0] dout;

always@(posedge reset or posedge clk)
begin
    if(reset)
        dout <= 4'b0000;
    else if(~start)
        dout <= 4'b0000;
    else if(load)
        dout <= data;
end
endmodule