//////////////////////////////////////////////////////////////////////////////////
//Author: Wesley Nguyen
//Date: November 25, 2018
//File Name: mult.v
//Purpose of Code: Assembles the individual steps for sequential multiplication 
//Project Part Number: Lab 2, Part 2, Sub-Part 2
//Hardware FPGA/CPLD Device: xc7a100tcsg324-1
//////////////////////////////////////////////////////////////////////////////////
module mult(A, ldA, B, ldB, sftB, sftF, ldF, start, reset, clk, product);
input               [3:0] A, B;
input               ldA, ldB, ldF, sftB, sftF,
                    clk, start, reset;
output              [7:0] product;
wire                multiplier_out, add_carry;
wire                [3:0] multiplicand_out, empty,
                    mux_out, add_out, ph_out;

assign              empty = 4'b0000;


shift_reg_b         step1(B, multiplier_out, ldB,
                          sftB, clk, start, reset);
                
reg_a               step2(clk, start, reset, ldA, A,
                          multiplicand_out);
                
mux                 step3(multiplicand_out, empty,
                          multiplier_out, mux_out);
                
adder               step4(mux_out, ph_out, add_out, add_carry);

shift_reg_final     step5(add_out, product, add_carry, ldB, ldF,
                          sftF, clk, start, reset, ph_out );
endmodule