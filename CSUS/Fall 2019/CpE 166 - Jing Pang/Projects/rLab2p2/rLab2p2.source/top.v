//////////////////////////////////////////////////////////////////////////////////
//Author: Wesley Nguyen
//Date: November 25, 2018
//File Name: top.v
//Purpose of Code: Top file for sequential multiplier 
//Project Part Number: Lab 2, Part 2, Sub-Part 1
//Hardware FPGA/CPLD Device: xc7a100tcsg324-1
//////////////////////////////////////////////////////////////////////////////////
module top(A, B, dout, clk, reset, start);
input       clk, reset, start;
input       [3:0] A;
input       [3:0] B;
output      [7:0] dout;
wire        ldA, ldB, sftB, ldF, sftF;

mult        multiply(A, ldA, B, ldB, sftB, sftF, ldF, start, reset, clk, dout);
fsm         FSMachine(clk, reset, ldA, ldB, ldF, sftB, sftF, start);
endmodule