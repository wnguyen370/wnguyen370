//////////////////////////////////////////////////////////////////////////////////
//Author: Wesley Nguyen
//Date: November 25, 2018
//File Name: adder.v
//Purpose of Code: Adds two numbers, stores and outputs the carry as well
//Project Part Number: Lab 2, Part 2, Sub-Part 6
//Hardware FPGA/CPLD Device: xc7a100tcsg324-1
//////////////////////////////////////////////////////////////////////////////////
module adder(a, b, sum, cout);
input       [3:0] a, b;
output      [3:0] sum;
output      cout;

assign      {cout, sum} = a + b;
endmodule