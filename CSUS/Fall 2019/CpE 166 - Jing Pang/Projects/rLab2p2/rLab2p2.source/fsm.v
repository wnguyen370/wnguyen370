//////////////////////////////////////////////////////////////////////////////////
//Author: Wesley Nguyen
//Date: November 25, 2018
//File Name: fsm.v
//Purpose of Code: Top file for sequential multiplier 
//Project Part Number: Lab 2, Part 2, Sub-Part 8
//Hardware FPGA/CPLD Device: xc7a100tcsg324-1
//////////////////////////////////////////////////////////////////////////////////
module fsm(clk, reset, ldA, ldB, ldF, sftB, sftF, start);
input       clk, reset, start;
output      reg ldA, ldB, ldF, sftB, sftF;
reg         [3:0] cs, ns;
parameter   idle = 0, s1 = 1, s2 = 2, s3 = 3, s4 = 4, s5 = 5, s6 = 6,
            s7 = 7, s8 = 8, s9 = 9, s10 = 10, s11 = 11, s12 = 12, s13 = 13;
            
always@(posedge clk or posedge reset)
begin
    if(reset)
        cs <= idle;
    else if (~start)
        cs <= s1;
    else
        cs <= ns;
end
always@(*)
begin
    case(cs)
        idle:       ns <= s1;
        s1:         ns <= s2;
        s2:         ns <= s3;
        s3:         ns <= s4;
        s4:         ns <= s5;
        s5:         ns <= s6;
        s6:         ns <= s7;
        s7:         ns <= s8;
        s8:         ns <= s9;
        s9:         ns <= s10;
        s10:        ns <= s11;
        s11:        ns <= s12;
        s12:        ns <= s13;
        s13:        ns <= s13;
        default:    ns <= idle;
    endcase
end
always@(*)
begin
    case(cs)
        idle:       begin ldA = 0; ldB = 0; ldF = 0; sftB = 0; sftF = 0; end
        s1:         begin ldA = 1; ldB = 1; ldF = 0; sftB = 0; sftF = 0; end
        s2:         begin ldA = 0; ldB = 0; ldF = 1; sftB = 0; sftF = 0; end
        s3:         begin ldA = 0; ldB = 0; ldF = 0; sftB = 0; sftF = 1; end
        s4:         begin ldA = 0; ldB = 0; ldF = 0; sftB = 1; sftF = 0; end
        s5:         begin ldA = 0; ldB = 0; ldF = 1; sftB = 0; sftF = 0; end
        s6:         begin ldA = 0; ldB = 0; ldF = 0; sftB = 0; sftF = 1; end
        s7:         begin ldA = 0; ldB = 0; ldF = 0; sftB = 1; sftF = 0; end
        s8:         begin ldA = 0; ldB = 0; ldF = 1; sftB = 0; sftF = 0; end
        s9:         begin ldA = 0; ldB = 0; ldF = 0; sftB = 0; sftF = 1; end
        s10:        begin ldA = 0; ldB = 0; ldF = 0; sftB = 1; sftF = 0; end
        s11:        begin ldA = 0; ldB = 0; ldF = 1; sftB = 0; sftF = 0; end
        s12:        begin ldA = 0; ldB = 0; ldF = 0; sftB = 0; sftF = 1; end
        s13:        begin ldA = 0; ldB = 0; ldF = 0; sftB = 0; sftF = 0; end
        default:    begin ldA = 0; ldB = 0; ldF = 0; sftB = 0; sftF = 0; end
    endcase
end
endmodule