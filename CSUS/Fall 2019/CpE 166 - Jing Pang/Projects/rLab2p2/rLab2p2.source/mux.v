//////////////////////////////////////////////////////////////////////////////////
//Author: Wesley Nguyen
//Date: November 25, 2018
//File Name: mux.v
//Purpose of Code: Multiplexer
//Project Part Number: Lab 2, Part 2, Sub-Part 5
//Hardware FPGA/CPLD Device: xc7a100tcsg324-1
//////////////////////////////////////////////////////////////////////////////////
module mux(data, empty, select, dout);
input       [3:0] data, empty;
input       select;
output      [3:0] dout;

reg         [3:0] dout;

always@(data or empty or select)
begin
    if(select)
        dout <= data;
    else
        dout <= empty;
    end
endmodule