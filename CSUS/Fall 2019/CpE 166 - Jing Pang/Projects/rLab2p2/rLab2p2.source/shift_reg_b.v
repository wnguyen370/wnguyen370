//////////////////////////////////////////////////////////////////////////////////
//Author: Wesley Nguyen
//Date: November 25, 2018
//File Name: Bshifter.v
//Purpose of Code: Shift register for input B; also acts as a D-Flip-Flop
//Project Part Number: Lab 2, Part 2, Sub-Part 3
//Hardware FPGA/CPLD Device: xc7a100tcsg324-1
//////////////////////////////////////////////////////////////////////////////////
module shift_reg_b(data, dout, load, shift, clk, start, reset);
input       start, reset, clk, load, shift;
input       [3:0] data;
output      [3:0] dout;

reg         [3:0] dout;

always@(posedge reset or posedge clk)
begin
    if(reset)
        dout <= 4'b0000;
    else if(~start)
        dout <= 4'b0000;
    else if(load)
        dout <= data;
    else if(shift)
    begin
        dout <= {1'b0, dout[3:1]};
    end
end
endmodule