//////////////////////////////////////////////////////////////////////////////////
//Author: Wesley Nguyen
//Date: November 25, 2018
//File Name: Pshifter.v
//Purpose of Code: Shift register dedicated solely for manipulating the output
//Project Part Number: Lab 2, Part 2, Sub-Part 7
//Hardware FPGA/CPLD Device: xc7a100tcsg324-1
//////////////////////////////////////////////////////////////////////////////////
module shift_reg_final(data, out, cin, ldB, load, shift, clk, start, reset, ph);
input       start, reset, clk, ldB, load, shift, cin;
input       [3:0] data;
output      [7:0] out;
output      [3:0] ph;

reg         [8:0] dout;

always@(posedge reset or posedge clk)
begin
    if(reset)
        dout <= 9'b00000000;
    else if(load)
        dout <= {cin, data, dout[3:0]};
    else if(shift)
        begin
            dout <= {cin, dout[8:1]};
        end
    else if(start & ldB)
        dout <= 9'b00000000;
end

assign      ph = dout [7:4];
assign      out = dout[7:0];
endmodule