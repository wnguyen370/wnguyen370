//////////////////////////////////////////////////////////////////////////////////
//Author: Wesley Nguyen
//Date: November 25, 2018
//File Name: top_tb.v
//Purpose of Code: top.v teshbench
//Project Part Number: Lab 2, Part 2, Sub-Part 9
//Hardware FPGA/CPLD Device: xc7a100tcsg324-1
//////////////////////////////////////////////////////////////////////////////////
`timescale 1ns / 1ps
module top_tb();
reg     [3:0] A, B;
reg     clk, reset, start;

wire    [7:0] dout;
top     uut(A, B, dout, clk, reset, start);

initial clk = 0;
always #10 clk = ~clk;

initial
begin
    reset = 0; start = 1;
    A = 4'b1111; B = 4'b1111;
end
endmodule
