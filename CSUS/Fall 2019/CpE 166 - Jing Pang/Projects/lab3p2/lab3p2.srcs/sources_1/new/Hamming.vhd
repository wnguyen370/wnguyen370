---------------------------------------------------------------------------------
--Author: Wesley Nguyen
--Date: December 9, 2018
--File Name: Hamming.vhd
--Purpose of Code: Convert 4-bit Input into a 7-bit Hamming Output
--Project Part Number: Lab 3, Part 2, Sub-Part 5
--Hardware FPGA/CPLD Device: xc7a100tcsg324-1
---------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
entity Hamming is
port(DFFin:         in std_logic_vector (3 downto 0);
     Hammingout:    out std_logic_vector (6 downto 0));
end Hamming;

architecture Behavioral of Hamming is
signal p1, p2, p4:  std_logic;

begin
process(DFFin)
begin
    p1 <= (DFFin(0) xor DFFin(1) xor DFFin(3));
    p2 <= (DFFin(0) xor DFFin(2) xor DFFin(3));
    p4 <= (DFFin(1) xor DFFin(2) xor DFFin(3));
    Hammingout <= (DFFin(3) & DFFin(2) & DFFin(1) & p4 & DFFin(0) & p2 & p1);
end process;

end Behavioral;
