---------------------------------------------------------------------------------
--Author: Wesley Nguyen
--Date: December 9, 2018
--File Name: DFF.vhd
--Purpose of Code: D Flip-Flop
--Project Part Number: Lab 3, Part 2, Sub-Part 4
--Hardware FPGA/CPLD Device: xc7a100tcsg324-1
---------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
entity DFF is
port(LFSRin:        in std_logic_vector (3 downto 0);
     clk1hz:        in std_logic;
     fsmClear:      in std_logic;
     DFFout:        out std_logic_vector (3 downto 0));
end DFF;

architecture Behavioral of DFF is
begin
process(clk1hz, LFSRin)
begin
    if (rising_edge(clk1hz)) then
        if (fsmClear = '1') then
            DFFout <= "0000";
        else
            DFFout <= LFSRin;
        end if;
    end if;
end process;

end Behavioral;
