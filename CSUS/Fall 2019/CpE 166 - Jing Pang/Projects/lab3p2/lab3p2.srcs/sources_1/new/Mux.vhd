---------------------------------------------------------------------------------
--Author: Wesley Nguyen
--Date: December 9, 2018
--File Name: Mux.vhd
--Purpose of Code: Multiplexer
--Project Part Number: Lab 3, Part 2, Sub-Part 7
--Hardware FPGA/CPLD Device: xc7a100tcsg324-1
---------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
entity Mux is
port(fsmState:      in std_logic_vector (1 downto 0);
     DFFin:         in std_logic_vector (3 downto 0);
     Hammingout:    in std_logic_vector (6 downto 0);
     ledout:        out std_logic_vector (6 downto 0));
end Mux;

architecture Behavioral of Mux is
signal mux:         std_logic_vector (6 downto 0);

begin
process(fsmState)
begin
    case (fsmState) is
    --reset/idle
    when "00" =>
        mux <= "0000000";
    --LFSR
    when "01" =>
        mux <= ("000" & DFFin);
    --Hamming
    when "10" =>
        mux <= Hammingout;
    --default
    when others =>
        mux <= "0000000";
    end case;
end process;

ledout <= mux;

end Behavioral;
