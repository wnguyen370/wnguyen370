---------------------------------------------------------------------------------
--Author: Wesley Nguyen
--Date: December 9, 2018
--File Name: FSM.vhd
--Purpose of Code: FSM
--Project Part Number: Lab 3, Part 2, Sub-Part 6
--Hardware FPGA/CPLD Device: xc7a100tcsg324-1
---------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
entity FSM is
port(switch1,switch2,switch3,clk1hz:    in std_logic;
     fsmClear:                          out std_logic;
     fsmState:                          out std_logic_vector (1 downto 0));
end entity;

architecture Behavioral of FSM is
type state_type is (idle, display_lfsr, hold_lfsr, display_ham);
signal cs, ns:                          state_type;

begin
reset: process(clk1hz)
begin
    if(rising_edge(clk1hz)) then
        if(switch1='1') then cs<=idle;
        else cs<=ns;
        end if;
    end if;
end process reset;

switch: process(cs, switch1, switch2, switch3)
begin
    case cs is 
    when idle =>
        if switch2 = '1' then ns <= display_lfsr;
        elsif switch3 = '1' then ns <= display_ham;
        else ns <= idle;
        end if;
    when display_lfsr =>
        if switch2 = '1' then ns <= display_lfsr;
        elsif switch3 = '1' then ns <= display_ham;
        elsif switch2 = '0' then ns <= hold_lfsr;
        else ns <= idle;
        end if;
    when hold_lfsr =>
        if switch2 = '1' then ns <= display_lfsr;
        elsif switch3 = '1' then ns <= display_ham;
        elsif switch2 = '0' then ns <= hold_lfsr;
        else ns <= idle;
        end if;
    when display_ham =>
        if switch3 = '1' then ns <= display_ham;
        elsif switch2 = '1' then ns <= display_lfsr;
        else ns <= idle;
        end if;
    end case;
end process switch;

process(cs)
begin
    case cs is
    when idle => 
        fsmClear <= '1';
        if(switch1 = '1') then fsmState <= "01";
        else fsmState <= "00";
        end if;
    when display_lfsr =>
        fsmClear <= '0'; 
        if(switch2 = '1') then fsmState <= "10";
        else fsmState <= "00";
        end if;
    when hold_lfsr =>
        fsmClear <= '0'; 
        if(switch2 = '0') then fsmState <= "10";
        else fsmState <= "00";
        end if;
    when display_ham => 
        fsmClear <= '0';
        if(switch3 = '1') then fsmState <= "11";
        else fsmState <= "00";
        end if;
    when others => fsmState <= "00"; fsmClear <= '0';
    end case;
end process;
end Behavioral;
