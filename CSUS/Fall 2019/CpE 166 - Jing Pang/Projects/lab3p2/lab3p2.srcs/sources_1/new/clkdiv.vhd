---------------------------------------------------------------------------------
--Author: Wesley Nguyen
--Date: December 9, 2018
--File Name: clkdiv.vhd
--Purpose of Code: CLock Divider
--Project Part Number: Lab 3, Part 2, Sub-Part 2
--Hardware FPGA/CPLD Device: xc7a100tcsg324-1
---------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
entity clkdiv is
port(clk:           in std_logic;
     clk1hz:        out std_logic);
end clkdiv;

architecture Behavioral of clkdiv is
signal cnt_div:     std_logic_vector (28 to 0);

begin
process(clk)
begin
if rising_edge (clk) then
    if(cnt_div = 99_999_999) then
        cnt_div <= (others => '0');
        clk1hz <= '1';
    elsif (cnt_div < 49_999_999) then
        cnt_div <= cnt_div + 1;
        clk1hz <= '1';
    else
        cnt_div <= cnt_div + 1;
        clk1hz <= '0';
    end if;
end if;
end process;
end Behavioral;
