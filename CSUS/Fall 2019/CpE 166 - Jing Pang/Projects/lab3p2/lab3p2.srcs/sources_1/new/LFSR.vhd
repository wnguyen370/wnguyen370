---------------------------------------------------------------------------------
--Author: Wesley Nguyen
--Date: December 9, 2018
--File Name: LFSR.vhd
--Purpose of Code: Linear Feeback Shift Register with Logic: x^4 + x + 1
--Project Part Number: Lab 3, Part 2, Sub-Part 3
--Hardware FPGA/CPLD Device: xc7a100tcsg324-1
---------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
entity LFSR is
port(clk1hz:        in std_logic;
     fsmClear:      in std_logic;
     LFSRout:       out std_logic_vector (3 downto 0));
end LFSR;

architecture Behavioral of LFSR is
signal temp:        std_logic_vector (3 downto 0);

begin
process(clk1hz, fsmClear)
begin
    if (fsmClear = '1') then
        temp <= (1 => '1', others => '0');
    elsif rising_edge(clk1hz) then
        temp <= (temp(2 downto 1) & (temp(0) xor temp(3)) & temp(3));
    end if;
end process;

LFSRout <= temp;

end Behavioral;
