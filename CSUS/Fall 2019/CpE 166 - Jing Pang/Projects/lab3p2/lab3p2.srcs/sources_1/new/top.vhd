---------------------------------------------------------------------------------
--Author: Wesley Nguyen
--Date: December 9, 2018
--File Name: top.vhd
--Purpose of Code: Top File
--Project Part Number: Lab 3, Part 2, Sub-Part 1
--Hardware FPGA/CPLD Device: xc7a100tcsg324-1
---------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
entity top is
Port(
clk:                            in std_logic;
switch1, switch2, switch3:      in std_logic;

led:                            out std_logic_vector (6 downto 0)
);
end top;

architecture Behavioral of top is
signal clk1hz, fsmClear: std_logic;
signal LFSRout, DFFout: std_logic_vector (3 downto 0);

signal fsmState: std_logic_vector (1 downto 0);
signal Hammingout, ledout: std_logic_vector (6 downto 0);

component clkdiv
port(
clk:                            in std_logic;
clk1hz:                         out std_logic
);
end component;

component LFSR
port(
clk1hz, fsmClear:               in std_logic;
LFSRout:                        out std_logic_vector (3 downto 0)
);
end component;

component FSM
port(
switch1, switch2, switch3:      in std_logic;
clk1hz:                         in std_logic;
fsmClear:                       out std_logic;
fsmState:                       out std_logic_vector (1 downto 0)
);
end component;

component DFF
port(
LFSRin:                         in std_logic_vector (3 downto 0);
clk1hz, fsmClear:               in std_logic;
DFFout:                         out std_logic_vector (3 downto 0)
);
end component;

component Hamming
port(
DFFin:                          in std_logic_vector (3 downto 0);
Hammingout:                     out std_logic_vector (6 downto 0)
);
end component;

component Mux
port(
fsmState:                       in std_logic_vector (1 downto 0);
DFFin:                          in std_logic_vector (3 downto 0);
Hammingout:                     in std_logic_vector (6 downto 0);
ledout:                         out std_logic_vector (6 downto 0)
);
end component;

--Top
begin
function0:  clkdiv port map(clk => clk, clk1hz => clk1hz);
function1:  LFSR port map(clk1hz => clk1hz, fsmClear => fsmClear, LFSRout => LFSRout);
function2:  FSM port map(switch1 => switch1, switch2 => switch2, switch3 => switch3,
                         clk1hz => clk1hz, fsmClear => fsmClear, fsmState => fsmState);
function3:  DFF port map(LFSRin => LFSRout, clk1hz => clk1hz, fsmClear => fsmClear, DFFout => DFFout);
function4:  Hamming port map(DFFin => DFFout, Hammingout => Hammingout);
function5:  Mux port map(fsmState => fsmState, DFFin => DFFout, Hammingout => Hammingout, ledout => ledout);
led <= ledout;

end Behavioral;
