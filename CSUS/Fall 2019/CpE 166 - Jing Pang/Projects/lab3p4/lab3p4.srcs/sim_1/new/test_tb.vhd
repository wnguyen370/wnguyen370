---------------------------------------------------------------------------------
--Author: Wesley Nguyen
--Date: December 9, 2018
--File Name: test_tb.vhd
--Purpose of Code: Testbench
--Project Part Number: Lab 3, Part 4, Sub-Part 4
--Hardware FPGA/CPLD Device: xc7a100tcsg324-1
---------------------------------------------------------------------------------

--**NOTE: In order to use ILA to capture the data, the filepath (C:\\...etc.) MUST BE LESS THAN 143 chracters long**

library IEEE;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
entity test_tb is
end test_tb;

architecture Behavioral of test_tb is
component top_sram
port(clk:               in std_logic;
     reset:             in std_logic;
     data:              inout std_logic_vector (3 downto 0));
end component;

signal clk_tb:          std_logic;
signal reset_tb:        std_logic;
signal data_tb:         std_logic_vector (3 downto 0);
constant clk_per:       time:= 20 ns;
begin
uut:                    top_sram port map(clk => clk_tb, reset => reset_tb, data => data_tb);
process
begin
    reset_tb <= '1';
    wait for 5 ns;
    reset_tb <= '0';
    wait for 100 ns;
    wait;
end process;
process
begin
    clk_tb <= '0';
    wait for clk_per/2;
    clk_tb <= '1';
    wait for clk_per/2;
    clk_tb <= '0';
    wait for clk_per/2;
    clk_tb <= '1';
    wait for clk_per/2;
    clk_tb <= '0';
    wait for clk_per/2;
    clk_tb <= '1';
    wait for clk_per/2;
    clk_tb <= '0';
    wait for clk_per/2;
    clk_tb <= '1';
    wait for clk_per/2;
    clk_tb <= '0';
    wait for clk_per/2;
    clk_tb <= '1';
    wait for clk_per/2;
end process;
end Behavioral;
