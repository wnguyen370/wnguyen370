library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity seven_seg_vdhl is
Port(clk: in std_logic;
load: in std_logic;
updown: in std_logic;
din: in std_logic_vector (3 downto 0);
cntout: out std_logic_vector (3 downto 0);
led: out std_logic_vector (3 downto 0);
clkout: out std_logic);
end seven_seg_vdhl;

architecture behavioral of seven_seg_vdhl is
--internal signals
signal cnt_div: std_logic_vector (28 downto 0);
signal cnt: std_logic_vector (3 downto 0);
signal clk2: std_logic;

begin

process(clk)
begin
if rising_edge (clk) then
    if(cnt_div = 99_999_999) then
        cnt_div <= (others => '0');
        clk2 <= '1';
    elsif (cnt_div < 49_999_999) then
        cnt_div <= cnt_div + 1;
        clk2 <= '1';
    else
        cnt_div <= cnt_div + 1;
        clk2 <= '0';
    end if;
end if;

end process;

process (clk2, load, updown) --process active on event of clk2,load, or updown
begin
if(load = '1') then
    cnt <= din;
elsif rising_edge (clk2) then
    if(updown = '1') then
        cnt <= cnt + 1;
    else
        cnt <= cnt - 1;
    end if;
end if;
end process;



cntout <= cnt; --output count
clkout <= clk2;--output clk
led <= cnt; --led output value
end behavioral;