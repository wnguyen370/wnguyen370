`timescale 1ns / 1ps

module mux( s, d0, d1, y);
    input s, d0, d1;
    output y;
    wire m, n;
    assign m = ~s & d0;
    assign n = s & d1;
    assign y = m | n;
endmodule
