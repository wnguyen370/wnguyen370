`timescale 1ns / 1ps


module mux_tb();
    reg s, d0, d1;
    wire y;
    mux uut(s, d0, d1, y);
    initial 
    begin
        s = 1'b0; d0=1'b0; d1=1'b0;
        #20;
        s=1'b0; d0=1'b0; d1=1'b1;
        #20;
        s=1'b0; d0=1'b1; d1=1'b0;
        #20;
        s=1'b0; d0=1'b1; d1=1'b1;
        #20;
        s=1'b1; d0=1'b0; d1=1'b0;
        #20;
        s=1'b1; d0=1'b0; d1=1'b1;
        #20;
        s=1'b1; d0=1'b1; d1=1'b0;
        #20;
        s=1'b1; d0=1'b1; d1=1'b1;
        #20 $stop;
    end
endmodule