---------------------------------------------------------------------------------
--Author: Wesley Nguyen
--Date: December 9, 2018
--File Name: mux2to1.vhd
--Purpose of Code: 2 Input Multiplexer
--Project Part Number: Lab 4, Sub-Part 5
--Hardware FPGA/CPLD Device: xc7a100tcsg324-1
---------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity mux2to1 is
port(d0:        in std_logic;
     d1:        in std_logic;
     s:         in std_logic;
     y:         out std_logic);
end mux2to1;

architecture behavior of mux2to1 is

begin
process(d0, d1, s)
begin
    if( s = '1' ) then
        y <= d1;
    else
        y <= d0;
    end if;
end process;
end behavior;