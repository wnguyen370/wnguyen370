---------------------------------------------------------------------------------
--Author: Wesley Nguyen
--Date: December 9, 2018
--File Name: dffs.vhd
--Purpose of Code: D Flip-Flop w/ Load and Clear
--Project Part Number: Lab 3, Part 3, Sub-Part 4
--Hardware FPGA/CPLD Device: xc7a100tcsg324-1
---------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
entity dffs is
port(load,clr:      in std_logic;
     data_lfsr:     in std_logic_vector (3 downto 0);
     out_lfsr:      out std_logic_vector (3 downto 0));
end dffs;

architecture beh3 of dffs is

begin
process(clr,load)
begin
    if(clr = '1') then
        out_lfsr <= "0000";
    elsif(load ='1') then
        out_lfsr <= data_lfsr;
    end if;
end process;
end beh3;