---------------------------------------------------------------------------------
--Author: Wesley Nguyen
--Date: December 9, 2018
--File Name: encoder.vhd
--Purpose of Code: Encoder
--Project Part Number: Lab 3, Part 3, Sub-Part 6
--Hardware FPGA/CPLD Device: xc7a100tcsg324-1
---------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
entity encoder is
port(q:         in std_logic_vector(4 downto 1);
     d3,d4:     out std_logic_vector(2 downto 0));
end encoder;

architecture beh9 of encoder is

begin 
    d3<= "0"& q(2) & q(1);
    d4<= "0"& q(4) & q(3);
end beh9;