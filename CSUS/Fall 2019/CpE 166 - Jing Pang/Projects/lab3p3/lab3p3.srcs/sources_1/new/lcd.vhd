---------------------------------------------------------------------------------
--Author: Wesley Nguyen
--Date: December 9, 2018
--File Name: lcd.vhd
--Purpose of Code: Seven-Seg Display Logic
--Project Part Number: Lab 3, Part 3, Sub-Part 8
--Hardware FPGA/CPLD Device: xc7a100tcsg324-1
---------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
entity lcd is
port(clk,clr, load:     in std_logic;
	 din:               in std_logic_vector (2 downto 0);
	 seg:               out std_logic_vector (7 downto 0);
     dis:               out std_logic_vector (7 downto 0));
end lcd;

architecture arch of lcd is
signal an:              std_logic_vector (7 downto 0);
begin
process (clk)
begin
    seg(7) <= '1'; 
    if(clr='1') then
        seg(6 downto 0) <= "1111111";
    elsif(load='1') then
        seg(6 downto 0) <= "1111111";
    else
        case(din) is 
        when x"0" => seg(6 downto 0) <= "1000000";      --to display 0
        when x"1" => seg(6 downto 0) <= "1111001";      --to display 1
        when x"2" => seg(6 downto 0) <= "0100100";      --to display 2
        when x"3" => seg(6 downto 0) <= "0110000";      --to display 3
        when x"4" => seg(6 downto 0) <= "0011001";      --to display 4
        when x"5" => seg(6 downto 0) <= "0010010";      --to display 5
        when x"6" => seg(6 downto 0) <= "0000010";      --to display 6
        when others => seg(6 downto 0) <= "1111111";    --blank
        end case;
    end if;
end process;
end arch;