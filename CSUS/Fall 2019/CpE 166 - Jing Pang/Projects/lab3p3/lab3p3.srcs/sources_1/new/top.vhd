---------------------------------------------------------------------------------
--Author: Wesley Nguyen
--Date: December 9, 2018
--File Name: top.vhd
--Purpose of Code: Combinational Multiplier
--Project Part Number: Lab 3, Part 3, Sub-Part 1
--Hardware FPGA/CPLD Device: xc7a100tcsg324-1
---------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
entity top is
port(osc, b1, b2,b3,b4:     in std_logic;
     led:                   out std_logic_vector (3 downto 0);
     seg:                   out std_logic_vector (7 downto 0);
     an:                    out std_logic_vector (7 downto 0));
end top;

architecture beh7 of top is
-- internal wire names for mapping
signal clk,clr,load:	   std_logic;
signal d3,d4:	           std_logic_vector (2 downto 0);

signal data_lfsr,out_lfsr: std_logic_vector (3 downto 0);
signal din:                std_logic_vector (2 downto 0);

component clkdiv
port(osc:                  in std_logic;
     clk:                  out std_logic);
end component;

component lfsr
port(clr, clk:             in std_logic;
	 q:                    out std_logic_vector (4 downto 1));
end component;

component dffs
port(load,clr:             in std_logic;
     data_lfsr:            in std_logic_vector (3 downto 0);
     out_lfsr:             out std_logic_vector (3 downto 0));
end component;

component fsm
port(b1,b2,clk:            in std_logic;
	 load,clr:             out std_logic);
end component;

component encoder
port(q:                    in std_logic_vector (4 downto 1);
     d3,d4:                out std_logic_vector (2 downto 0));
end component;

component decoder
port(b3,b4:                in std_logic;
     d3,d4:                in std_logic_vector (2 downto 0);
     din:                  out std_logic_vector (2 downto 0));
end component;

component lcd
port(clk,clr, load:        in std_logic;
	 din:                  in std_logic_vector (2 downto 0);
	 seg:                  out std_logic_vector (7 downto 0);
     dis:                  out std_logic_vector (7 downto 0));
end component;


begin
g1:     clkdiv port map(osc => osc,clk => clk);
g2:     fsm port map(b1 => b1, b2 => b2,clk => clk,load => load,clr => clr);
g3:     lfsr port map(clr => clr,clk => clk,q => data_lfsr);
g4:     dffs port map(load => load,clr => clr,data_lfsr => data_lfsr,out_lfsr => out_lfsr);
g5:     encoder port map(q => out_lfsr,d3 => d3,d4 => d4);
g6:     decoder port map(b3 => b3,b4 => b4,d3 => d3, d4 => d4,din => din);
g7:     lcd port map (clk => clk,clr => clr,load => load,din => din,seg => seg, dis => an);

led <= out_lfsr;
an <= "11111110";
end beh7;