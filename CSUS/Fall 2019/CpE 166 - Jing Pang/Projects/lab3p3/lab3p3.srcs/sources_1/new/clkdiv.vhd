---------------------------------------------------------------------------------
--Author: Wesley Nguyen
--Date: December 9, 2018
--File Name: clkdiv.vhd
--Purpose of Code: Clock Divider
--Project Part Number: Lab 3, Part 3, Sub-Part 2
--Hardware FPGA/CPLD Device: xc7a100tcsg324-1
---------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
entity clkdiv is
port(osc:       in std_logic;	
     clk:       out std_logic);
end clkdiv;

architecture beh1 of clkdiv is 
signal cnt:     std_logic_vector(25 downto 0);

begin
process(osc)
begin
    if (rising_edge(osc)) then
        if (cnt = 99_999_999) then          
            cnt <= (others=>'0');
            clk <= '1';
        elsif( cnt < 49_999_999) then    
            cnt <= cnt + 1;
            clk <= '1';
        else
            cnt <= cnt + 1;
            clk <= '0';
        end if;
    end if;
end process;
end beh1;