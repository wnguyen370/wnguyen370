---------------------------------------------------------------------------------
--Author: Wesley Nguyen
--Date: December 9, 2018
--File Name: decoder.vhd
--Purpose of Code: Decoder
--Project Part Number: Lab 3, Part 3, Sub-Part 7
--Hardware FPGA/CPLD Device: xc7a100tcsg324-1
---------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
entity decoder is
port(b3,b4:         in std_logic;
     d3,d4:         in std_logic_vector (2 downto 0);
     din:           out std_logic_vector (2 downto 0));
end decoder;

architecture beh92 of decoder is
signal t1: std_logic_vector(1 downto 0);

begin 
t1 <= (b4 & b3);

process (b3, b4)
    begin
    case (t1) is
    when "00" => 
        if(d3 < d4) then 
            din <= "001";
        else
            din <= "000";
        end if;
    when "01" => din <= (d4 + d3);
    when "10" => din <= (d4 - d3);
    when "11" => din <= (d4 nand d3); din(2) <= 'X';
    when others => din <= "000";
    end case;
end process;
end beh92;