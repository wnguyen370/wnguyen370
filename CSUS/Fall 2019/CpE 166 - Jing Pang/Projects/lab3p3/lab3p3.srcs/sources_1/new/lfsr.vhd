---------------------------------------------------------------------------------
--Author: Wesley Nguyen
--Date: December 9, 2018
--File Name: lfsr.vhd
--Purpose of Code: Linear Feeback Shift Register with Logic: x^4 + x + 1
--Project Part Number: Lab 3, Part 3, Sub-Part 3
--Hardware FPGA/CPLD Device: xc7a100tcsg324-1
---------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
entity lfsr is
port(clr, clk:      in std_logic;
     q:             out std_logic_vector (4 downto 1));
end lfsr;

architecture beh2 of lfsr is
signal w:           std_logic_vector(4 downto 1);

begin
process(clr,clk)
begin
    if (clr='1') then
        w <= (1 => '1', others => '0' );
    elsif (rising_edge(clk)) then
        w <= w(3 downto 2) & (w(1) xor w(4)) & w(4);
    end if;
end process;

q <= w;

end beh2; 