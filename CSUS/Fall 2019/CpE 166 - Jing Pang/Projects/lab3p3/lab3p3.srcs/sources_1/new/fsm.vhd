---------------------------------------------------------------------------------
--Author: Wesley Nguyen
--Date: December 9, 2018
--File Name: encoder.vhd
--Purpose of Code: FSM
--Project Part Number: Lab 3, Part 3, Sub-Part 5
--Hardware FPGA/CPLD Device: xc7a100tcsg324-1
---------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
entity fsm is
port(b1,b2,clk:         in std_logic;
	 load, clr:         out std_logic);
end fsm;

architecture beh5 of fsm is
type state_type is (idle, display_lfsr, hold_lfsr);
signal cs, ns: state_type;

begin
reset: process(clk) begin
    if(rising_edge(clk)) then
        if(b1='1') then 
            cs<=idle;
        else
            cs<=ns;
        end if;
    end if;
end process reset;

switch: process(cs,b1,b2)begin
    case cs is 
    when idle =>
        if b2 = '1' then ns <= display_lfsr;
        else ns <= idle;
        end if;
    when display_lfsr =>
        if b2 = '1' then ns <= display_lfsr;
        elsif b2 = '0' then ns <= hold_lfsr;
        else ns <= idle;
        end if;
    when hold_lfsr =>
        if b2 = '1' then ns <= display_lfsr;
        elsif b2 = '0' then ns <= hold_lfsr;
        else ns <= idle;
        end if;
    end case;
end process switch;

outp: process(cs) begin
    case cs is
    when idle => load<= '0'; clr <= '1';
    when display_lfsr => load<= '1'; clr <= '0'; 
    when hold_lfsr => load<= '0'; clr <= '0'; 
    when others => load<= '0'; clr <= '0';
    end case;
end process outp;
end beh5;