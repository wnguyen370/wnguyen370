//////////////////////////////////////////////////////////////////////////////////
//Author: Wesley Nguyen
//Date: November 25, 2018
//File Name: multiplier_tb.v
//Purpose of Code: Combinational Multiplier testbench
//Project Part Number: Lab 1, Part 1, Sub-part 2
//Hardware FPGA/CPLD Device: Not Used
//////////////////////////////////////////////////////////////////////////////////
`timescale 1ns / 1ps
module multiplier_tb();
    reg [3:0] a, b;
    wire [7:0] p;
    integer i, j;
    multiplier uut(a, b, p);
    initial
    begin
        a = 4'b0000; b = 4'b0000;
        for (i = 0; i < 16; i = i + 1)
        begin
            for (j = 0; j < 16; j = j + 1)
            begin
                a = i;
                b = j;
                #1;
            end
        end
        #1 $stop;
    end
endmodule
