//////////////////////////////////////////////////////////////////////////////////
//Author: Wesley Nguyen
//Date: November 25, 2018
//File Name: multiplier.v
//Purpose of Code: Combinational Multiplier
//Project Part Number: Lab 1, Part 1, Sub-Part 1
//Hardware FPGA/CPLD Device: Not Used
//////////////////////////////////////////////////////////////////////////////////
module ha(a, b, c, s);
    input a, b;
    output c, s;
    assign c = a & b,
           s = a ^ b;
endmodule


module fa(a, b, cin, cout, s);
    input a, b, cin;
    output cout, s;
    wire m, n, p;
    ha ha1(a, b, n, m);
    ha ha2(.a(cin), .b(m), .c(p), .s(s));
    assign cout = n | p;
endmodule


module multiplier(x, y, p);
    input [3:0] x, y;
    output [7:0] p;
    wire [3:0] r0, r1, r2, r3,
               c0, c1, c2,
               s0, s1, s2;
    assign p[0] = r0[0],
           p[1] = s0[0],
           p[2] = s1[0],
           p[3] = s2[0],
           p[4] = s2[1],
           p[5] = s2[2],
           p[6] = s2[3],
           p[7] = c2[3];
           
    assign r0[0] = x[0] & y[0], r0[1] = x[1] & y[0], r0[2] = x[2] & y[0], r0[3] = x[3] & y[0],
           r1[0] = x[0] & y[1], r1[1] = x[1] & y[1], r1[2] = x[2] & y[1], r1[3] = x[3] & y[1],
           r2[0] = x[0] & y[2], r2[1] = x[1] & y[2], r2[2] = x[2] & y[2], r2[3] = x[3] & y[2],
           r3[0] = x[0] & y[3], r3[1] = x[1] & y[3], r3[2] = x[2] & y[3], r3[3] = x[3] & y[3];
           
    ha ha0(r0[1], r1[0], c0[0], s0[0]);
    fa fa0(r0[2], r1[1], c0[0], c0[1], s0[1]);
    fa fa1(r0[3], r1[2], c0[1], c0[2], s0[2]);
    ha ha1(r1[3], c0[2], c0[3], s0[3]);
    
    ha ha2(r2[0], s0[1], c1[0], s1[0]);
    fa fa2(r2[1], s0[2], c1[0], c1[1], s1[1]);
    fa fa3(r2[2], s0[3], c1[1], c1[2], s1[2]);
    fa fa4(r2[3], c0[3], c1[2], c1[3], s1[3]);
    
    ha ha3(r3[0], s1[1], c2[0], s2[0]);
    fa fa5(r3[1], s1[2], c2[0], c2[1], s2[1]);
    fa fa6(r3[2], s1[3], c2[1], c2[2], s2[2]);
    fa fa7(r3[3], c1[3], c2[2], c2[3], s2[3]);
endmodule
